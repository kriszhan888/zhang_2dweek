﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoinPicker : MonoBehaviour
{
    private float coin = 0;
    public TextMeshProUGUI CoinCount;
    public GameObject winTextObject;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.transform.tag == "Coin")
        {
            coin ++;
            CoinCount.text = "Coin:" + coin.ToString();
            Destroy(other.gameObject);

            if(coin >= 5)
            {
                winTextObject.SetActive(true);
            }
        }
    }
    private void Start()
    {
        winTextObject.SetActive(false);
    }
}
