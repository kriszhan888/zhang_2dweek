﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameEnding : MonoBehaviour
{
    Rigidbody2D rb2d;
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public float speed;
    public int seconds;
    public int second;
    public TextMeshProUGUI Timer;
    public float FreezeCooldown = 10f;
    private float nextFreezeTime = 0;
    public TextMeshProUGUI Cooldown;
    private float cooldown = 10f;

    public float timeLeft = 3f;


    bool m_IsPlayerAtExit;
    float m_Timer;

   void OnTriggerEnter2D(Collider2D other)
    {
       if(other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }
    void Update()
    {
        if (m_IsPlayerAtExit)
        {
            EndGame();
        }

        timeLeft -= Time.deltaTime;
        cooldown -= Time.deltaTime;
        SetTimerText();
        if (timeLeft <= 0)
        {
            //Ran out of time;
            EndGame();
        }
        if (Time.time > nextFreezeTime)
        {
         
            if (Input.GetKeyDown(KeyCode.Q))
            {
                FreezeWall();
                nextFreezeTime = Time.time + FreezeCooldown;
            }
        }
    }

    void SetTimerText()
    {
        seconds = (int)(timeLeft % 60);
        Timer.text = ("timeLeft:" + seconds + " " + "Sec");
    }
    void EndGame()
    {
        m_Timer += Time.deltaTime;
        exitBackgroundImageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            Application.Quit();
        }
    }
    void Start()
    {

        rb2d = GetComponent<Rigidbody2D>();

        rb2d.velocity = Vector2.right * speed;

    }
    void FreezeWall()
    {
        rb2d.velocity = Vector2.zero;
        CancelInvoke("EndFreeze");
        Invoke("EndFreeze", 5f);
        SetCooldownText();

    }
    void SetCooldownText()
    {
        second = (int)(cooldown % 60);
        Cooldown.text = ("timeLeft:" + second + " " + "Sec");
    }
    void EndFreeze()
    {
        rb2d.velocity = Vector2.right * speed;
    }
}
